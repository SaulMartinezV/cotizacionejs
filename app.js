const http = require('http')
const express = require('express');
const res = require('express/lib/response');
const bodyParser = require("body-parser");
const app = express()
const port = 3002
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({extenden:true}))

let datos = [{
matricula: "2020030240",
nombre: "Saul Valdes",
sexo: "M", 
materias:["ingles", "Tecnologias de internet I", "Base de datos"]
},

{
    matricula: "2020030244",
    nombre: "Saul Martinez",
    sexo: "M", 
    materias:["ingles I", "Tecnologias de internet II", "Base de datos"]


},

{
    matricula: "2020030249",
    nombre: "Juan Morelos",
    sexo: "M", 
    materias:["ingles", "Tecnologias de internet I", "Base de datos"]

},
{

    matricula: "2020030299",
    nombre: "Genjiro Quintero",
    sexo: "M", 
    materias:["ingles", "Tecnologias de internet I", "Base de datos"]


},
{

    matricula: "2020030296",
    nombre: "Luis Santillan",
    sexo: "M", 
    materias:["ingles", "Tecnologias de internet I", "Base de datos"]


},
{
    matricula: "2020030554",
nombre: "Guillermo Chavez",
sexo: "M", 
materias:["ingles", "Tecnologias de internet I", "Base de datos"]
},
{
    matricula: "2020030206",
    nombre: "Juan Moreno",
    sexo: "M", 
    materias:["ingles", "Tecnologias de internet I", "Base de datos"]
}
]

app.get('/cotizacionM', (req,res)=>{

    const val ={
        //para llamar valores necesarios
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.body.plazos
    }
    
    res.render('cotizacionM', val)
    
    })
    //post y get de cotizacion
    app.post('/cotizacionM', (req,res)=>{
    
        const val ={
            //para llamar valores necesarios
            valor:req.body.valor,
            pInicial:req.body.pInicial,
            plazos:req.body.plazos
        }
        res.render('cotizacionM', val)
        
        })
    

app.get('/',(req,res)=>{
    //res.send('Iniciamos con express')
    res.render('index',{titulo:'Listado Alumnos',listado:datos})
    
})

app.get('/',(req,res)=>{
    //res.send('Iniciamos con express')
    res.render('index',{titulo:'Listado Alumnos',listado:datos})
    
})
app.get('/tablas',(req,res)=>{
   const valores = {

    tabla:req.query.tablas

   }
    res.render('tablas', valores)
})
app.post("/tablas",(req, res)=>{

const valores = {

    tablas:req.query.tablas
}
res.render('tablas', valores)
})


app.use((req,res,next)=>{

//error hasta le final
res.status(404).sendFile(__dirname + '/public/errorM.html');


})
app.use(express.static(__dirname + '/public'))

app.listen(port,()=>{
    console.log('Inciado el puerto 3002')
})
